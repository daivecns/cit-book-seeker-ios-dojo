import Foundation
import Alamofire

protocol Requestable {
    
    var url: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get set }
    
}

extension Requestable {
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
}
