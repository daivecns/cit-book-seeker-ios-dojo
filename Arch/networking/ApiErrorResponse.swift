import Foundation

public struct ApiErrorResponse : Codable {
    
    let code: Int
    let message: String
    
    public init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
    
    public init() {
        code = -1
        message = ""
    }
    
}
