import UIKit
import RxSwift
import Kingfisher
import RatingControl

class BookDetailViewController: BaseViewController<BookDetailViewModel, BookSeekerCoordinator> {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    private func configure() {
        bindUI()
        viewModel.loadBookDetails()
    }

    private func bindUI() {
        viewModel.loadedBook.asObservable().skip(1).subscribe(onNext: { [unowned self] book in
            if let book = book {
                self.title = book.title
                let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
                    NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                    NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
                ]

                let attrStr = try? NSAttributedString(
                    data: (book.description.data(using: String.Encoding.unicode, allowLossyConversion: true)!),
                    options: options,
                    documentAttributes: nil
                )

                self.lblTitle.text = book.title
                self.lblAuthor.text = book.author
                self.lblDescription.attributedText = attrStr
                self.ratingControl.rating = book.rating
                self.lblRating.text = "\(book.rating)"
                self.imgCover.kf.setImage(with: URL(string: book.imageHD), placeholder: UIImage(named: "AppIcon"))
            }
        }).disposed(by: viewModel.bag)
    }

}
