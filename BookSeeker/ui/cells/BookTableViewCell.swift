import UIKit
import Kingfisher

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    static var cellIdentifier: String {
        return String(describing: self)
    }

    func configure(withBook book: Book) {
        lblTitle.text = book.title
        lblDescription.text = book.author
        imageBook.kf.indicatorType = .activity
        imageBook.kf.setImage(with: URL(string: book.image), placeholder: UIImage(named: "AppIcon"))
    }

}
