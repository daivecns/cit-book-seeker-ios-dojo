import UIKit

class LastSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSearchString: UILabel!

    static var cellIdentifier: String {
        return String(describing: self)
    }

    func configure(withSearchString searchString: String) {
        lblSearchString.text = searchString
    }

}
