import UIKit
import RxSwift

class BookSearchViewController: BaseViewController<BookSearchViewModel, BookSeekerCoordinator> {

    @IBOutlet weak var bookSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    private var timer: Timer?
    private var userSearches: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.isHidden = true

        bookSearchBar.text = ""
        getLastSearches()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.navigationController?.navigationBar.isHidden = false
    }

    private func configure() {
        bookSearchBar.searchBarStyle = .minimal

        bindUI()
    }

    private func getLastSearches() {
        viewModel.listUserSearches()
        self.tableView.reloadData()
    }

    private func bindUI() {
        viewModel.booksToDisplay.asObservable().skip(1).subscribe(onNext: { [unowned self] books in
            self.recordSearch(self.bookSearchBar.text!)
            self.coordinator.showBookList(books: books)
        }).disposed(by: viewModel.bag)

        viewModel.searchesToDisplay.asObservable().skip(1).subscribe(onNext: { searches in
            self.userSearches = searches
        }).disposed(by: viewModel.bag)
    }

    private func recordSearch(_ text: String) {
        let searchText = text.trimmingCharacters(in: .whitespaces)
        if !userSearches.contains(searchText) {
            userSearches.insert(searchText, at: 0)
            viewModel.storeUserSearches(term: searchText)
        }
    }

    private func startTimer(userInfo: Any?) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self,
                                     selector: #selector(self.searchBarTextDidEndEditing),
                                     userInfo: userInfo, repeats: false)
    }

    @objc private func searchBarTextDidEndEditing(timer: Timer) {
        let searchBar = timer.userInfo as? UISearchBar
        let title = searchBar!.text!
        if title.count >= 3 {
            view.endEditing(true)
            viewModel.listBooks(title: title)
        }
    }

}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BookSearchViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userSearches.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: LastSearchTableViewCell.cellIdentifier)
            as? LastSearchTableViewCell {
            let searchItem = userSearches[indexPath.row]
            cell.configure(withSearchString: searchItem)
            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        bookSearchBar.text = userSearches[indexPath.row]
        startTimer(userInfo: bookSearchBar)
    }
}

// MARK: - UISearchBarDelegate
extension BookSearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        startTimer(userInfo: searchBar)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }

}
