import UIKit
import RxRelay
import RxSwift
import Kingfisher

class BookListViewController: BaseViewController<BookListViewModel, BookSeekerCoordinator> {

    @IBOutlet weak var tableView: UITableView!

    private var bookList: [Book] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    private func configure() {
        tableView.delegate = self
        tableView.dataSource = self

        navigationController?.navigationBar.isHidden = false

        bindUI()

        viewModel.loadBooks()
    }

    private func bindUI() {
        viewModel.booksToDisplay.asObservable().skip(1).subscribe(onNext: { [unowned self] books in
            self.bookList = books
        }).disposed(by: viewModel.bag)
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension BookListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: BookTableViewCell.cellIdentifier)
            as? BookTableViewCell {
            let book = bookList[indexPath.row]
            cell.configure(withBook: book)
            return cell
        } else {
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = self.bookList[indexPath.row]
        self.coordinator.showBookDetail(book: book)
    }

}
