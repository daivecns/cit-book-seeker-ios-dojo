import Foundation

public struct Book {
    var title: String
    var author: String
    var description: String
    var image: String
    var imageHD: String
    var price: String?
    var currency: String
    var kind: String
    var releaseDate: Date
    var rating: Double
}
