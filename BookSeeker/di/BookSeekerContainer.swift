import Foundation
import Swinject

class BookSeekerContainer {

    let container = Container()

    func setup() {

        container.register(BookRepositoryProtocol.self) { _  in BookRepository() }

        container.register(ListBooksUseCase.self) { resolver in
            let repository = resolver.resolve(BookRepositoryProtocol.self)
            return ListBooksUseCase(repository: repository!)
        }

        container.register(BookSearchViewModel.self) { resolver in
            let useCase = resolver.resolve(ListBooksUseCase.self)
            return BookSearchViewModel(listBooksUseCase: useCase!)
        }

    }

    public func mockSetup() {

        container.register(BookRepositoryProtocol.self) { _  in BookMockRepository() }

        container.register(ListBooksUseCase.self) { resolver in
            let repository = resolver.resolve(BookRepositoryProtocol.self)
            return ListBooksUseCase(repository: repository!)
        }

    }

    public init() { }

    public init(mockSetup: Bool) {
        if mockSetup {
            self.mockSetup()
        } else {
            setup()
        }
    }
}
