import Foundation

public protocol BookRepositoryProtocol {
    func list(title: String, completion: @escaping ([Book]?, ApiErrorResponse?) -> Void )
}
