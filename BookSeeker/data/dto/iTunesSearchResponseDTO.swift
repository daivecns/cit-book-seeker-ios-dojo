import Foundation

struct ItunesSearchResponseDTO: Codable {
    let resultCount: Int
    let results: [BookDTO]
}
